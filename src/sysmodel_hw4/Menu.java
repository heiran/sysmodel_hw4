package sysmodel_hw4;

import sysmodel_hw4.Restaurant;

import java.util.ArrayList;
import java.util.List;

public class Menu {
	Restaurant restaurant;
	String dish_name;
	int dish_calories;
	String dish_quality;
	String beverage_name;
	int beverage_volume;
	String beverage_quality;
	int dish_cost;
	int beverage_cost;

	int lowDishPrice = 0;
	int highDishPrice = 0;
	int lowBeveragePrice = 0;
	int highBeveragePrice = 0;

	public Menu(String dish_name, int dish_calories, String dish_quality,
			int dish_cost, String beverage_name, int beverage_volume,
			String beverage_quality, int beverage_cost) {
		this.beverage_name = beverage_name;
		this.beverage_quality = beverage_quality;
		this.beverage_volume = beverage_volume;
		this.dish_name = dish_name;
		this.dish_quality = dish_quality;
		this.dish_calories = dish_calories;
		this.dish_cost = dish_cost;
		this.beverage_cost = beverage_cost;
	}

	public String getDish_name() {
		return dish_name;
	}

	public void setDish_name(String dish_name) {
		this.dish_name = dish_name;
	}

	public int getDish_calories() {
		return dish_calories;
	}

	public void setDish_calories(int dish_calories) {
		this.dish_calories = dish_calories;
	}

	public int getDish_cost() {
		return dish_cost;
	}

	public void setDish_cost(int dish_cost) {
		this.dish_cost = dish_cost;
	}

	public int getBeverage_cost() {
		return beverage_cost;
	}

	public void setBeverage_cost(int beverage_cost) {
		this.beverage_cost = beverage_cost;
	}

	public int getLowDishPrice() {
		return lowDishPrice;
	}

	public void setLowDishPrice(int lowDishPrice) {
		this.lowDishPrice = lowDishPrice;
	}

	public int getHighDishPrice() {
		return highDishPrice;
	}

	public void setHighDishPrice(int highDishPrice) {
		this.highDishPrice = highDishPrice;
	}

	public int getLowBeveragePrice() {
		return lowBeveragePrice;
	}

	public void setLowBeveragePrice(int lowBeveragePrice) {
		this.lowBeveragePrice = lowBeveragePrice;
	}

	public int getHighBeveragePrice() {
		return highBeveragePrice;
	}

	public void setHighBeveragePrice(int highBeveragePrice) {
		this.highBeveragePrice = highBeveragePrice;
	}

	public List getMenu() {
		return menu;
	}

	public void setMenu(List menu) {
		this.menu = menu;
	}

	public String getDish_quality() {
		return dish_quality;
	}

	public void setDish_quality(String dish_quality) {
		this.dish_quality = dish_quality;
	}

	public String getBeverage_name() {
		return beverage_name;
	}

	public void setBeverage_name(String beverage_name) {
		this.beverage_name = beverage_name;
	}

	public int getBeverage_volume() {
		return beverage_volume;
	}

	public void setBeverage_volume(int beverage_volume) {
		this.beverage_volume = beverage_volume;
	}

	public String getBeverage_quality() {
		return beverage_quality;
	}

	public void setBeverage_quality(String beverage_quality) {
		this.beverage_quality = beverage_quality;
	}

	public int getDishCost() {
		return dish_cost;
	}

	List menu;

	public String createMenu(ArrayList menuItems) {
		for (int i = 0; i < menuItems.size(); i++) {
			Menu menuItem = (Menu) menuItems.get(i);
			if ("low".equals(menuItem.getDish_quality())) {
				if (lowDishPrice == 0) {
					lowDishPrice = menuItem.getDishCost();
				} else if (menuItem.getDishCost() != 0) {
					if (menuItem.getDishCost() != lowDishPrice) {
						return "ERROR!!! All the low quality dishes must have the same price";
					}
				}
			}
			if ("high".equals(menuItem.getDish_quality())) {
				if (highDishPrice == 0) {
					highDishPrice = menuItem.getDishCost();
				} else if (menuItem.getDishCost() != 0) {
					if (menuItem.getDishCost() != highDishPrice) {
						return ("ERROR!!! All the high quality dishes must have the same price");
					}
				}

			}
			if ("low".equals(menuItem.getBeverage_quality())) {
				if (lowBeveragePrice == 0) {
					lowBeveragePrice = menuItem.getBeverageCost();
				} else if (menuItem.getBeverageCost() != 0) {
					if (menuItem.getBeverageCost() != lowBeveragePrice) {
						return ("ERROR!!! All the low quality beverages must have the same price");
					}
				}
			}
			if ("high".equals(menuItem.getBeverage_quality())) {
				if (highBeveragePrice == 0) {
					highBeveragePrice = menuItem.getBeverageCost();
				} else if (menuItem.getBeverageCost() != 0) {
					if (menuItem.getBeverageCost() != highBeveragePrice) {
						return ("ERROR!!! All the high quality beverages must have the same price");
					}
				}
			}
		}
		return "Restaurant menu is created";
	}

	private int getBeverageCost() {
		return beverage_cost;
	}
	
	
	public String calculate(ArrayList orders){
		int budget = restaurant.getBudget();
		for (int i = 0; i < orders.size(); i++){
			Menu order = (Menu) orders.get(i);
			if (budget != 0){
				budget=budget - order.getDish_cost();
			}
			else{
				return ("The budget is: " + budget + "\n Game over!");
			}
		}
		return ("The budget is: " + budget);
	}

}

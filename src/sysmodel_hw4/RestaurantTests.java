package sysmodel_hw4;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import sysmodel_hw4.Restaurant;

public class RestaurantTests {
	Restaurant restaurant;
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

	@Before
	public void setUpStreams() {
		System.setOut(new PrintStream(outContent));
		System.setErr(new PrintStream(errContent));
	}

	@After
	public void cleanUpStreams() {
		System.setOut(null);
		System.setErr(null);
	}

	@Test
	public void TestRestaurantGameFeatures() {
		restaurant = new Restaurant();
		restaurant.setName("UT Restaurant (UTR)");
		restaurant.setPlayer("Naved");
		restaurant.setBudget(10000);
		String response = restaurant.start();
		assertTrue("Welcome to UT Restaurant (UTR) Game!\nThank you, Naved.\nThe starting budget equals to 10000."
				.equals(response));

	}

	@Test
	public void TestMenuIsCreated() {
		Menu dish1 = new Menu("grilled chicken", 400, "high", 12, null, 0,
				null, 0);
		Menu dish2 = new Menu("lasagna", 800, "low", 7, null, 0, null, 0);
		Menu dish3 = new Menu("gnocchi", 700, "high", 12, null, 0, null, 0);
		Menu dish4 = new Menu("pizza", 400, "low", 7, null, 0, null, 0);
		Menu dish5 = new Menu("snitzel", 700, "high", 12, null, 0, null, 0);
		Menu beverage1 = new Menu(null, 0, null, 0, "coke", 35, "high", 5);
		Menu beverage2 = new Menu(null, 0, null, 0, "fanta", 35, "low", 2);
		Menu beverage3 = new Menu(null, 0, null, 0, "wine", 50, "high", 5);
		Menu beverage4 = new Menu(null, 0, null, 0, "beer", 50, "low", 2);
		Menu beverage5 = new Menu(null, 0, null, 0, "sprite", 35, "high", 5);

		ArrayList arrayList = new ArrayList();
		arrayList.add(dish1);
		arrayList.add(dish2);
		arrayList.add(dish3);
		arrayList.add(dish4);
		arrayList.add(dish5);
		arrayList.add(beverage1);
		arrayList.add(beverage2);
		arrayList.add(beverage3);
		arrayList.add(beverage4);
		arrayList.add(beverage5);
		String response = dish1.createMenu(arrayList);

		assertTrue("Restaurant menu is created".equals(response));
	}

	@Test
	public void TestFailingHighQualityDish() {
		Menu dish1 = new Menu("grilled chicken", 400, "high", 12, null, 0,
				null, 0);
		Menu dish2 = new Menu("lasagna", 800, "low", 7, null, 0, null, 0);
		Menu dish3 = new Menu("gnocchi", 700, "high", 10, null, 0, null, 0);
		Menu dish4 = new Menu("pizza", 400, "low", 7, null, 0, null, 0);
		Menu dish5 = new Menu("snitzel", 700, "high", 12, null, 0, null, 0);

		ArrayList arrayList = new ArrayList();
		arrayList.add(dish1);
		arrayList.add(dish2);
		arrayList.add(dish3);
		arrayList.add(dish4);
		arrayList.add(dish5);
		String response = dish1.createMenu(arrayList);

		assertTrue("ERROR!!! All the high quality dishes must have the same price"
				.equals(response));
	}

	@Test
	public void TestFailingLowQualityDish() {
		Menu dish1 = new Menu("grilled chicken", 400, "high", 12, null, 0,
				null, 0);
		Menu dish2 = new Menu("lasagna", 800, "low", 7, null, 0, null, 0);
		Menu dish3 = new Menu("gnocchi", 700, "high", 12, null, 0, null, 0);
		Menu dish4 = new Menu("pizza", 400, "low", 6, null, 0, null, 0);
		Menu dish5 = new Menu("snitzel", 700, "high", 12, null, 0, null, 0);

		ArrayList arrayList = new ArrayList();
		arrayList.add(dish1);
		arrayList.add(dish2);
		arrayList.add(dish3);
		arrayList.add(dish4);
		arrayList.add(dish5);
		String response = dish1.createMenu(arrayList);

		assertTrue("ERROR!!! All the low quality dishes must have the same price"
				.equals(response));
	}

	@Test
	public void TestFailingHighQualityBeverage() {
		Menu beverage1 = new Menu(null, 0, null, 0, "coke", 35, "high", 7);
		Menu beverage2 = new Menu(null, 0, null, 0, "fanta", 35, "low", 2);
		Menu beverage3 = new Menu(null, 0, null, 0, "wine", 50, "high", 5);
		Menu beverage4 = new Menu(null, 0, null, 0, "beer", 50, "low", 2);
		Menu beverage5 = new Menu(null, 0, null, 0, "sprite", 35, "high", 5);

		ArrayList arrayList = new ArrayList();
		arrayList.add(beverage1);
		arrayList.add(beverage2);
		arrayList.add(beverage3);
		arrayList.add(beverage4);
		arrayList.add(beverage5);
		String response = beverage1.createMenu(arrayList);

		assertTrue("ERROR!!! All the high quality beverages must have the same price"
				.equals(response));
	}

	@Test
	public void TestFailingLowQualityBeverage() {
		Menu beverage1 = new Menu(null, 0, null, 0, "coke", 35, "high", 5);
		Menu beverage2 = new Menu(null, 0, null, 0, "fanta", 35, "low", 1);
		Menu beverage3 = new Menu(null, 0, null, 0, "wine", 50, "high", 5);
		Menu beverage4 = new Menu(null, 0, null, 0, "beer", 50, "low", 2);
		Menu beverage5 = new Menu(null, 0, null, 0, "sprite", 35, "high", 5);

		ArrayList arrayList = new ArrayList();
		arrayList.add(beverage1);
		arrayList.add(beverage2);
		arrayList.add(beverage3);
		arrayList.add(beverage4);
		arrayList.add(beverage5);
		String response = beverage1.createMenu(arrayList);

		assertTrue("ERROR!!! All the low quality beverages must have the same price"
				.equals(response));
	}

	@Test
	public void TestUpdateBudget1() {
		Menu highQualityDish = new Menu(null, 0, "high", 10, null, 0, null, 0);
		Menu lowQualityDish = new Menu(null, 0, "low", 3, null, 0, null, 0);
		Menu highQualityBeverage = new Menu(null, 0, null, 0, null, 0, "high", 3);
		Menu lowQualityBeverage = new Menu(null, 0, null, 0, null, 0, "low", 3);
		ArrayList arrayList = new ArrayList();
		restaurant.setBudget(6000);
		for (int i = 0; i < 4; i++) {
			Menu temp = highQualityDish;
			arrayList.add(temp);
			System.out.println(arrayList);
		}
		String response = highQualityDish.createMenu(arrayList);
		
		System.out.println(arrayList);

		//menu.calculate(arrayList);

	}

}

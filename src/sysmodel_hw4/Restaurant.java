package sysmodel_hw4;

public class Restaurant {
	int reputation;
	String name;
	String player;
	int budget;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPlayer() {
		return player;
	}
	public void setPlayer(String player) {
		this.player = player;
	}
	public int getBudget() {
		return budget;
	}
	public void setBudget(int budget) {
		this.budget = budget;
	}
	public void setReputation(int reputation) {
		this.reputation = reputation;
	}
	public void recordClientSatisfactionOnFood() {
		reputation++;
	}
	public void recordClientSatisfactionOnBeverage() {
		reputation++;
	}
	public void recordClientSatisfactionOnService() {
		reputation++;
	}
	public void recordClientDissatisfactionOnFood() {
		reputation--;
	}
	public void recordClientDissatisfactionOnBeverage() {
		reputation--;
	}
	public void recordClientDissatisfactionOnService() {
		reputation--;
	}
	public void updateReputation() {
	}
	public int getReputation() {
		return reputation;
	}

	

	public String start(){
		return ("Welcome to " + getName() + " Game!\nThank you, " + getPlayer() + ".\nThe starting budget equals to " + getBudget() + ".");
	}
}
